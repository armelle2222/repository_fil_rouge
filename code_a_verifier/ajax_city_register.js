/**
 * Formulaire d'inscription
 * Ce morceau de code doit être adapté pour permettre de sélectionner une ville à partir des résultats de l'API
 * Problème rencontré: il détecte bien ce qui est tapé, mais n'affiche pas la liste déroulante
 * Les requêtes sont bien envoyées
 * Et les différents css/script étaient placés dans le fichier base.html.twig
 */

//<link rel="stylesheet" href="https://code.jquery.com/ui/1.7.3/themes/base/jquery-ui.css" />

//<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
//<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

$("#register_city_name").autocomplete({
	source: function (request, response) {
		$.ajax({
			url:
				"https://geo.api.gouv.fr/communes?nom=" +
				$("input[name='register[city][name]'").val() +
				"&fields=departement&boost=population&limit=5",
			data: { q: request.term },
			dataType: "json",
			success: function (data) {
				var postcodes = [];

				response(
					$.map(data.features, function (item) {
						// Ici on est obligé d'ajouter les CP dans un array pour ne pas avoir plusieurs fois le même
						if ($.inArray(item.properties.postcode, postcodes) == -1) {
							postcodes.push(item.properties.postcode);
							return {
								label: item.properties.postcode + " - " + item.properties.city,
								city: item.properties.city,
								value: item.properties.postcode,
							};
						}
					})
				);
			},
		});
	},

	// On remplit aussi la ville

	select: function (event, ui) {
		$("#ville").val(ui.item.city);
	},
});
