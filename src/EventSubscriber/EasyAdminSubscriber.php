<?php

namespace App\EventSubscriber;

use App\Entity\City;
use App\Entity\User;
use App\Entity\Plant;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{

	private $entityManager;
	private $passwordEncoder;
	private $slugger;


	/**
	 * Initialization of the entity manager and password hasher needed later
	 *
	 * @param  mixed $entityManager
	 * @param  mixed $passwordEncoder
	 * @param  mixed $slugger
	 * @return void
	 */
	public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordEncoder, SluggerInterface $slugger)
	{
		$this->entityManager = $entityManager;
		$this->passwordEncoder = $passwordEncoder;
		$this->slugger = $slugger;
	}
	/**
	 * With this method we can subscribe to different events and call a function which is executed when the subscribed event happens
	 *
	 * @return void
	 */
	public static function getSubscribedEvents()
	{
		return [
			BeforeEntityPersistedEvent::class => [['addUser'], ['addCity']],
		];
	}

	/**
	 * This function is called when we add an user with Easy Admin
	 *
	 * @param  mixed $event
	 * @return void
	 */
	public function addUser(BeforeEntityPersistedEvent $event)
	{
		//Verify if we are creating an User object
		$entity = $event->getEntityInstance();
		if (!($entity instanceof User)) {
			return;
		}
		$this->setPassword($entity);
		$this->setPseudo($entity);
		// $this->setAvatar($entity);

	}

	public function addCity(BeforeEntityPersistedEvent $event)
	{
		//Verify if we are creating an City object
		$entity = $event->getEntityInstance();
		if (!($entity instanceof City)) {
			return;
		}
		$this->setCity($entity);
	}

	public function setPassword(User $entity)
	{
		// encode the plain password
		$entity->setPassword(
			$this->passwordEncoder->hashPassword(
				$entity,
				$entity->getPassword()
			)
		);
		$this->entityManager->persist($entity);
		$this->entityManager->flush();
	}

	public function setPseudo(User $entity)
	{
		// clean the pseudo entry
		$pseudo = trim(strip_tags($entity->getPseudo()));
		$entity->setPseudo($pseudo);
		$this->entityManager->persist($entity);
		$this->entityManager->flush();
	}

	public function setCity(City $entity)
	{
		$name = ucfirst(trim(strip_tags($entity->getName())));
		$entity->setName($name);
		$this->entityManager->persist($entity);
		$this->entityManager->flush();
	}
	/* 
	public function setAvatar(User $entity)
	{
		$avatar = $entity->files->getAvatar();
		$safeFilename = $this->slugger->slug($avatar);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $avatar->guessExtension();
        $entity->setAvatar($newFilename);
	$this->entityManager->persist($entity);
    $this->entityManager->flush();
	} */
}
