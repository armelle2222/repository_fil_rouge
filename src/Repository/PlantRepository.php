<?php

namespace App\Repository;

use App\Entity\Plant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Plant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Plant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Plant[]    findAll()
 * @method Plant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Plant::class);
    }

    // /**
    //  * @return Plant[] Returns an array of Plant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function searchByName(string $name): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.name LIKE :name')
            ->setParameter('name', "$name%")
            ->getQuery()
            ->getResult();
    }

    public function searchByFilters(array $filters): array
    {
        $query = $this->createQueryBuilder('p');

        foreach ($filters as $groupFilter) {
            $partQuery = [];
//            dd($filters);
            foreach ($groupFilter as $filter => $value) {
//                if there are only one month
//                    dd($value);
                if(is_int($value)) {
                    $partQuery[] = "p.{$filter} = (" . $value . ")";
                } else {
                $partQuery[] = "p.{$filter} IN (" . implode(',', $value) . ")";
                }
            }
            $query->andWhere(implode(' OR ', $partQuery));
        }
//        dd($query->getQuery());
        return $query->getQuery()->getResult();
    }
}
