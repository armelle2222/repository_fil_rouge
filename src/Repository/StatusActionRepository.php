<?php

namespace App\Repository;

use App\Entity\StatusAction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatusAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusAction[]    findAll()
 * @method StatusAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusAction::class);
    }

    // /**
    //  * @return StatusAction[] Returns an array of StatusAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatusAction
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
