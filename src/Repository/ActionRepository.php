<?php

namespace App\Repository;

use App\Entity\Action;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Action|null find($id, $lockMode = null, $lockVersion = null)
 * @method Action|null findOneBy(array $criteria, array $orderBy = null)
 * @method Action[]    findAll()
 * @method Action[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Action::class);
    }

     /**
      * @return Action[] Returns an array of Action objects
      */

    public function searchByFilters(array $filters): array
    {
        $query = $this->createQueryBuilder('a');
        foreach ($filters as $groupFilter) {
            $partQuery = [];
            foreach ($groupFilter as $filter => $value) {
                if(is_int($value)) {
                    $partQuery[] = "a.{$filter} = (" . $value . ")";
               }else {
                    $partQuery[] = "a.{$filter} IN (" . implode(',', $value) . ")";
                }
            }
            $query->andWhere(implode(' OR ', $partQuery));
        }
        //dd($query->getQuery());
        return $query->getQuery()->getResult();
    }

    public function findAllUserActions($user)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.idUser = :val')
            ->setParameter('val', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findMinDateStart()
    {
        return $this->createQueryBuilder('s')
            ->select('MIN(s.dateStart)')
            ->getQuery()
            ->getResult()
            ;
    }
    public function findMaxDateStart()
    {
        return $this->createQueryBuilder('s')
            ->select('MAX(s.dateStart)')
            ->getQuery()
            ->getResult()
            ;
    }
    /*
    public function findOneBySomeField($value): ?Action
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
