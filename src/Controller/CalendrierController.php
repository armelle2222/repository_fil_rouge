<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalendrierController extends AbstractController
{
    #[Route('/calendrier', name: 'calendrier')]
    public function index(): Response
    {

        $title = "Calendrier des semis et récoltes";

        return $this->render('calendrier/calendrier.html.twig', [
            'controller_name' => 'CalendrierController',
            'title' => $title
        ]);
    }


        #[Route('/calendrier/automne', name: 'automne')]
    public function automne(): Response
    {

        $title = "légumes d'automne";

        return $this->render('calendrier/saison/automne.html.twig', [
            'controller_name' => 'CalendrierController',
            'title' => $title
        ]);
    }

    
        #[Route('/calendrier/ete', name: 'ete')]
    public function ete(): Response
    {
                $title = "légumes d'été";

        return $this->render('calendrier/saison/ete.html.twig', [
            'controller_name' => 'CalendrierController',
            'title' => $title
        ]);
    }

        #[Route('/calendrier/hiver', name: 'hiver')]
    public function hiver(): Response
    {
        $title = "légumes d'hiver";

        return $this->render('calendrier/saison/hiver.html.twig', [
            'controller_name' => 'CalendrierController',
            'title' => $title
        ]);
    }

        #[Route('/calendrier/printemps', name: 'printemps')]
    public function printemps(): Response
    {
        $title = "légumes d'printemps";

        return $this->render('calendrier/saison/printemps.html.twig', [
            'controller_name' => 'CalendrierController',
            'title' => $title
        ]);
    }
}
