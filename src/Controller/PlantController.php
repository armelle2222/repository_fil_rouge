<?php

namespace App\Controller;

use App\Entity\Plant;
use App\Entity\Ground;
use App\Repository\PlantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Repository\GroundRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class PlantController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/plant', name: 'plant')]
    public function index(PlantRepository $repository, Request $request): Response
    {
        $month = [
            'janvier' => 1,
            'février' => 2,
            'mars' => 3,
            'avril' => 4,
            'mai' => 5,
            'juin' => 6,
            'juillet' => 7,
            'août' => 8,
            'septembre' => 9,
            'octobre' => 10,
            'novembre' => 11,
            'decembre' => 12
        ];

        $plants = $repository->findAll();

        $form = $this->createFormBuilder()
            ->add('name', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'attr' => ['placeholder' => ' fruit / légume'],
                'label' => false
            ])
            ->add('submit', SubmitType::class, ['label' => 'Rechercher'])
            ->getForm();

        $filterForm = $this->createFormBuilder()
            ->add('sowingMonthStart', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices'  => $month,
                'required' => false,
                'attr' => [
                    'empty_data' => 'Aucun',
                    'default' => ''
                ]
            ])
            ->add('sowingMonthEnd', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices'  => $month,
                'required' => false,
            ])
            ->add('harvestMonthStart', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices'  => $month,
                'required' => false
            ])
            ->add('harvestMonthEnd', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices'  => $month,
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success']
            ])
            ->getForm();

        $form->handleRequest($request);
        $filterForm->handleRequest($request);

        $autocomplete = [];

        foreach ($repository->findAll() as $plant) {
            $autocomplete[] = $plant->getName();
        }

        //        if search by searchbar is submitted
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $plants = $repository->searchByName($data['name']);
        }

        //        if search by filters is submitted
        if ($filterForm->isSubmitted() && $filterForm->isValid()) {
            $data = $filterForm->getData();

            // verify if there are at least one filter
            if (!empty(array_filter($data, function ($a) {
                return $a !== null;
            }))) {
                $data = array_filter($data, function ($a) {
                    return $a !== null;
                });
                $data = $this->prepareData($data);
                $plants = $repository->searchByFilters($data);
            }
        }

        return $this->render('plant/index.html.twig', [
            'controller_name' => 'PlantController',
            'plants' => $plants,
            'autocomplete' => $autocomplete,
            'form' => $form->createView(),
            'filterForm' => $filterForm->createView()
        ]);
    }

    protected function prepareData(array $data)
    {
        if (isset($data['sowingMonthStart']) && isset($data['sowingMonthEnd'])) {
            $sowingRange = $this->getMonthRange($data['sowingMonthStart'], $data['sowingMonthEnd']);
            $returnedData[] = [
                'sowingMonthStart' => $sowingRange,
                'sowingMonthEnd' => $sowingRange
            ];
        } elseif (isset($data['sowingMonthStart']) || isset($data['sowingMonthEnd'])) {
            $sowingRange = (isset($data['sowingMonthStart'])) ? $data['sowingMonthStart'] : $data['sowingMonthEnd'];
            $returnedData[] = [
                'sowingMonthStart' => $sowingRange,
                'sowingMonthEnd' => $sowingRange
            ];
        }

        if (isset($data['harvestMonthStart']) && isset($data['harvestMonthEnd'])) {
            $harvestRange = $this->getMonthRange($data['harvestMonthStart'], $data['harvestMonthEnd']);
            $returnedData[] = [
                'harvestMonthStart' => $harvestRange,
                'harvestMonthEnd' => $harvestRange
            ];
        } elseif (isset($data['harvestMonthStart']) || isset($data['harvestMonthEnd'])) {
            $harvestRange = (isset($data['harvestMonthStart'])) ? $data['harvestMonthStart'] : $data['harvestMonthEnd'];
            $returnedData[] = [
                'harvestMonthStart' => $harvestRange,
                'harvestMonthEnd' => $harvestRange
            ];
        }

        return $returnedData;
        //        $data['sowingMonthStart'] = $sowingRange;
        //        $data['sowingMonthEnd'] = $sowingRange;
        //        $data['harvestMonthStart'] = $harvestRange;
        //        $data['harvestMonthEnd'] = $harvestRange;
    }


    protected function getMonthRange(int $start, int $end): array
    {
        //        create the intervalle between 2 months selected
        $range = [];

        //        get all months of the intervalle
        for ($i = $start; $i !== $end; $i++) {
            $i = $i % 13;
            if ($i === 0) $i++;
            $range[] = $i;
        }
        //        add the last month
        $range[] = $end;

        return $range;
    }

    #[Route('/plant/{id}', name: 'plant_show')]
    public function showOnePlant(Plant $plant, GroundRepository $groundRepository): Response
    {
        $user = $this->getUser();
        if ($user != null) {
            $ground = $groundRepository->findOneBy(['user' => $user->getId()]);
            $userPlants = $ground->getPlant();

            $userPlantsName = [];
            foreach ($userPlants as $userPlant) {
                $userPlantsName[] = $userPlant->getName();
            }

            $groundTypeUser = $ground->getGroundType();
            $groundTypesPlant = $plant->getGroundType();
            $compatibility = false;
            foreach ($groundTypesPlant as $item) {
                if ($item == $groundTypeUser) {
                    $compatibility = true;
                }
            }
        } else {
            $userPlantsName = null;
            $compatibility = null;
        }
        $months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
        return $this->render('plant/show.html.twig', [
            'plant' => $plant,
            'userPlantsName' => $userPlantsName,
            'compatibility' => $compatibility,
            'months' => $months,
            'user' => $user,
        ]);
    }
}
