<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('pseudo')
                ->setLabel('Pseudonyme'),
            TextField::new('password')
                ->setLabel('Mot de passe')
                ->hideOnIndex(),
            EmailField::new('email')
                ->setLabel('Adresse e-mail'),
            AssociationField::new('city')
                ->setLabel('Ville')
                ->setHelp('Si la ville n\'existe pas encore, vous devez la créer au préalable à partir du menu <a href="admin?crudAction=index&crudControllerFqcn=App\Controller\Admin\CityCrudController&menuIndex=4&signature=BnA8FzmRu4Vrmo5eJiuyM3SM0_hd00L688_YqnAlRzE&submenuIndex=-1"><b>Villes</b></a>'),
            ChoiceField::new('role')
                ->setChoices(['Administrateur' => 'ROLE_ADMIN', 'Utilisateur' => 'ROLE_USER', 'Compte désactivé' => 'ROLE_DISABLED'])
                ->setLabel('Rôle'),
            BooleanField::new('valider')
                ->setLabel('E-mail validé'),
            ImageField::new('avatar')
                ->setLabel('Avatar')
                ->setFormType(FileUploadType::class)
                ->setUploadDir('public/uploads/avatars/'),
        ];
    }
}
