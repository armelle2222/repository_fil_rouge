<?php

namespace App\Controller\Admin;

use App\Entity\City;
use App\Entity\User;
use App\Entity\Plant;
use App\Entity\Action;
use App\Entity\Ground;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\GroundPh;
use App\Entity\GroundType;
use App\Entity\WaterNeeds;
use App\Entity\Maintenance;
use App\Entity\SunExposure;
use App\Entity\ColdResistance;
use App\Entity\GroundMoisture;
use App\Entity\TypeAction;
use App\Entity\StatusAction;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<div class="col-3 col-md-3 col-lg-10 mx-auto"><img src="img/logo/logoPoussVert.png" alt="Logo" class="img-thumbnail"></div>')
            ->setFaviconPath('img/logo/logoPoussVert.png');

    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Accueil', 'fa fa-home');
        yield MenuItem::linkToLogout('Se déconnecter', 'fa fa-sign-out');
        yield MenuItem::section('Gestion générale');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-users', User::class);
        yield MenuItem::linkToCrud('Villes', 'fas fa-building', City::class);
        yield MenuItem::linkToCrud('Terrain de l\'utilisateur', 'fa fa-square-o', Ground::class);
        yield MenuItem::linkToCrud('Articles', 'fa fa-newspaper-o', Article::class);
        yield MenuItem::section('Gestion des plantes');
        yield MenuItem::linkToCrud('Plantes', 'fas fa-leaf', Plant::class);
        yield MenuItem::linkToCrud('Catégorie', 'fas fa-tags', Category::class);
        yield MenuItem::linkToCrud('Humidité du sol', 'fas fa-tint', GroundMoisture::class);
        yield MenuItem::linkToCrud('Ph du sol', 'fas fa-flask', GroundPh::class);
        yield MenuItem::linkToCrud('Type de sol', 'fa fa-diamond', GroundType::class);
        yield MenuItem::linkToCrud('Entretien', 'fa fa-scissors', Maintenance::class);
        yield MenuItem::linkToCrud('Exposition', 'fas fa-sun', SunExposure::class);
        yield MenuItem::linkToCrud('Arrosage', 'fas fa-shower', WaterNeeds::class);
        yield MenuItem::linkToCrud('Résistance au froid', 'fas fa-thermometer', ColdResistance::class);
        yield MenuItem::section('Gestion des actions');
        yield MenuItem::linkToCrud('Actions', 'fa fa-calendar-check-o', Action::class);
        yield MenuItem::linkToCrud('Type d\'action', 'fa fa-check-circle-o', TypeAction::class);
        yield MenuItem::linkToCrud('Statut des actions', 'fa fa-check-circle-o', StatusAction::class);
    }
}
