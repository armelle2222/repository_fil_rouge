<?php

namespace App\Controller\Admin;

use App\Entity\SunExposure;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SunExposureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SunExposure::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
