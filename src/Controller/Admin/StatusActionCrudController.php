<?php

namespace App\Controller\Admin;

use App\Entity\StatusAction;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StatusActionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StatusAction::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
