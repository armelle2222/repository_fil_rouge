<?php

namespace App\Controller\Admin;

use App\Entity\Ground;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GroundCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Ground::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
