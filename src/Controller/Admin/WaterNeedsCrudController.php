<?php

namespace App\Controller\Admin;

use App\Entity\WaterNeeds;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class WaterNeedsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return WaterNeeds::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
