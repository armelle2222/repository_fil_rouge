<?php

namespace App\Controller\Admin;

use App\Entity\GroundType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GroundTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return GroundType::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
