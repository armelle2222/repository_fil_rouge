<?php

namespace App\Controller\Admin;

use App\Entity\Plant;
use App\Entity\GroundPh;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PlantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Plant::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name')
                ->setLabel('Nom'),
            TextEditorField::new('text')
                ->setLabel('Description'),
            AssociationField::new('category')
                ->setLabel('Catégorie'),
            AssociationField::new('coldResistance')
                ->setLabel('Résistance au froid'),
            AssociationField::new('maintenance')
                ->setLabel('Entretien'),
            AssociationField::new('waterNeeds')
                ->setLabel('Arrosage'),
            TextField::new('spacing')
                ->setLabel('Espacement'),
            AssociationField::new('sunExposure', SunExposure::class)
                ->setLabel('Exposition'),
            AssociationField::new('groundPh', GroundPh::class)
                ->setLabel('Ph du sol'),
            AssociationField::new('groundMoisture', GroundMoisture::class)
                ->setLabel('Humidité du sol'),
            AssociationField::new('groundType', GroundType::class)
                ->setLabel('Type de sol'),
            ImageField::new('image')
                ->setLabel('Image')
                ->setFormType(FileUploadType::class)
                ->setUploadDir('public/img/images_bdd/'),
            IntegerField::new('sowing_month_start')
                ->setLabel('Début des semis'),
            IntegerField::new('sowing_month_end')
                ->setLabel('Fin des semis'),
            IntegerField::new('transplanting_month_start')
                ->setLabel('Début des plantations'),
            IntegerField::new('transplanting_month_end')
                ->setLabel('Fin des plantations'),
            IntegerField::new('harvest_month_start')
                ->setLabel('Début de la récolte'),
            IntegerField::new('harvest_month_end')
                ->setLabel('Fin de la récolte'),
        ];
    }
}
