<?php

namespace App\Controller\Admin;

use App\Entity\ColdResistance;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ColdResistanceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ColdResistance::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
