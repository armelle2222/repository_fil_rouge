<?php

namespace App\Controller\UserDashboard;

use App\Repository\GroundRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MeteoController extends AbstractController
{
    #[Route('/meteo', name: 'meteo')]
    public function index(GroundRepository $groundRepository): Response
    {
        //Activation de l'onglet Tâches
        $user = $this->getUser();
        $ground = $groundRepository->findOneBy(['user' => $user]);
        if($ground != null){
            $tasksButtonDisplay = true;
        }else{
            $tasksButtonDisplay = false;
        }

        return $this->render('meteo/meteo.html.twig', [
            'title' => 'La météo de votre potager',
            'tasksButtonDisplay' => $tasksButtonDisplay,
        ]);
    }
}
