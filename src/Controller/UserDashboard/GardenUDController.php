<?php

namespace App\Controller\UserDashboard;

use App\Entity\Ground;
use App\Form\GardenType;
use App\Repository\PlantRepository;
use App\Repository\GroundRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GardenUDController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/dashboard/garden', name: 'garden')]
    public function editGarden(PlantRepository $plantRepository, GroundRepository $groundRepository, Request $request)
    {
        $plants = [];
        $autocomplete = [];

        $form = $this->createFormBuilder()
            ->add('name', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'attr' => ['placeholder' => 'Cherchez un legume ou un fruit'],
                'label' => false
            ])
            ->add('submit', SubmitType::class, ['label' => 'Rechercher', 'attr' => ['class' => 'mt-2']])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $plants = $plantRepository->searchByName($data['name']);
        }
        $autocomplete = [];

        foreach ($plantRepository->findAll() as $plant) {
            $autocomplete[] = $plant->getName();
        }

        //Activation de l'onglet Tâches
        $user = $this->getUser();
        $ground = $groundRepository->findOneBy(['user' => $user]);
        if($ground != null){
            $tasksButtonDisplay = true;
        }else{
            $tasksButtonDisplay = false;
        }

        if ($groundRepository->findOneBy(['user' => $user->getId()])) {
            $ground = $groundRepository->findOneBy(['user' => $user->getId()]);
            $userPlants = $ground->getPlant();

            $userPlantsName = [];
            foreach ($userPlants as $userPlant) {
                $userPlantsName[] = $userPlant->getName();
            }
        } else {
            $userPlants = null;
            $userPlantsName = null;
        }

        return $this->render('userDashboard/garden.html.twig', [
            'title' => 'Mon jardin',
            'plants' => $plants,
            'userPlants' => $userPlants,
            'userPlantsName' => $userPlantsName,
            'autocomplete' => $autocomplete,
            'tasksButtonDisplay' => $tasksButtonDisplay,
            'form' => $form->createView(),
        ]);
    }

    #[Route('dashboard/garden/plant/add', name: 'plant_add')]
    public function addPlantInGarden(Request $request, GroundRepository $groundRepository, PlantRepository $plantRepository): Response
    {
        $plantId = $request->get('plant_id');

        if ($plantId === null) {
            return new Response(false);
        }

        $userId = $this->getUser()->getId();

        $userGround = $groundRepository->findOneBy(['user' => $userId]);
        if ($userGround !== null) {
            $plant = $plantRepository->findOneBy(['id' => $plantId]);
            $userGround->addPlant($plant);
            $this->entityManager->persist($userGround);
            $this->entityManager->flush();
            return new Response(true);
        } else {
            return new Response(false);
        }
    }

    #[Route('dashboard/garden/plant/remove', name: 'plant_remove')]
    public function removePlantInGarden(Request $request, GroundRepository $groundRepository, PlantRepository $plantRepository): Response
    {
        $plantId = $request->get('plant_id');

        if ($plantId === null) {
            return new Response(false);
        }

        $userId = $this->getUser()->getId();

        $userGround = $groundRepository->findOneBy(['user' => $userId]);

        if ($userGround !== null) {
            $plant = $plantRepository->findOneBy(['id' => $plantId]);
            $userGround->removePlant($plant);
            $this->entityManager->persist($userGround);
            $this->entityManager->flush();
            return new Response(true);
        } else {
            return new Response(false);
        }
    }

    #[Route('/dashboard/questionnaire', name: 'questionnaire')]
    public function questionnaire(Request $request, GroundRepository $groundRepository): Response
    {
        $ground = new Ground();
        $form = $this->createForm(GardenType::class, $ground);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $ground->setUser($user);
            $userId = $ground->getUser()->getId();

            $foundedGround = $groundRepository->findOneBy(['user' => $userId]);

            if ($foundedGround !== null) {
                $ground = $foundedGround;
            }

            $width = $form->get('width')->getData();
            $ground->setWidth($width);

            $height = $form->get('height')->getData();
            $ground->setHeight($height);

            $groundType = $form->get('groundType')->getData();
            $ground->setGroundType($groundType);


            $plants = $form->get('plant')->getData();
            foreach ($plants as $plant) {
                $ground->addPlant($plant);
            }


            $this->entityManager->persist($ground);
            $this->entityManager->flush();
            $this->addFlash('success', 'Votre jardin a bien été modifié.');
            return $this->redirectToRoute('dashboard');
        }
        return $this->render('userDashboard/questionnaire.html.twig', [
            'title' => 'Questionnaire',
            'form' => $form->createView(),
            'tasksButtonDisplay' => false,
        ]);
    }
}
