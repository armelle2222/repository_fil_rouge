<?php

namespace App\Controller\UserDashboard;

use App\Entity\Action;
use App\Repository\ActionRepository;
use App\Repository\GroundRepository;
use App\Repository\PlantRepository;
use App\Repository\StatusActionRepository;
use App\Repository\TypeActionRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function PHPUnit\Framework\exactly;

class TasksUDController extends AbstractController
{
    private $entityManager;
    private $actionRepository;
    private $typeActionRepository;
    private $statusActionRepository;
    private $plantRepository;
    private $client;
    private $groundRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        PlantRepository $plantRepository,
        TypeActionRepository $typeActionRepository,
        ActionRepository $actionRepository,
        StatusActionRepository $statusActionRepository,
        HttpClientInterface $client,
        GroundRepository $groundRepository
    ) {
        $this->entityManager = $entityManager;
        $this->plantRepository = $plantRepository;
        $this->typeActionRepository = $typeActionRepository;
        $this->actionRepository = $actionRepository;
        $this->statusActionRepository = $statusActionRepository;
        $this->client = $client;
        $this->groundRepository = $groundRepository;
    }

    #[Route('/dashboard/tasks', name: 'tasks')]
    public function index(Request $request): Response
    {
        $user = $this->getUser();
        $userGround = $this->groundRepository->findOneBy(['user' => $user]);
        $userPlants = $userGround->getPlant();
        $actionsUser = $this->actionRepository->findBy(['idUser' => $user->getId()]);

        $plantsWithAction = [];
        foreach($actionsUser as $action) {
            $plantWithAction = $action->getIdPlant()->getId();
            if(!in_array($plantWithAction, $plantsWithAction )) {
            $plantsWithAction[] = $action->getIdPlant()->getId();
            }
        }

        $newPlants = [];
        $newPlantsForm = [];
        $imageNewPlants = [];
        foreach($userPlants as $plant) {
            $plantId = $plant->getId();
            if(!in_array($plantId, $plantsWithAction)) {
                if($plant->getSowingMonthStart() == null) {
                    $this->addFirstActions($plantId, 'planter');
                } else if ($plant->getTransplantingMonthStart() == null) {
                    $this->addFirstActions($plantId, 'semer');
                } else {
                    $newPlants[$plantId] = $plant;
                    $newPlantsForm[$plant->getName()] = $plantId;
                    $imageNewPlants[$plantId] = $plant->getImage();
                }
            }
        }
        
        //---call this method only one time per day ( at 4pm for example )-----
//        $postalCode = $user->getCity()->getPostalCode();
//                if($postalCode == null) {
//                    $this->redirectToRoute(path('login'));
//                }
//        $this->verifcationEvent($postalCode);

        $user = $this->getUser();
        $userId = $user->getUserIdentifier();
        //initialisation of $userActions to display a table of actions
        $userActions = $this->actionRepository->findAllUserActions($userId);

        //configuration of choices Plantes
        $userGround = $this->groundRepository->findOneBy(['user' => $user]);

        //Activation de l'onglet Tâches
        if($userGround != null){
            $tasksButtonDisplay = true;
        }else{
            $tasksButtonDisplay = false;
        }

        $userPlants = $userGround->getPlant();
        $userPlantsName = [];
        foreach ($userPlants as $userPlant) {
            $userPlantsName[$userPlant->getName()] = $userPlant->getId();
        }
        //configuration of choices Statuts
        $actionStatus = $this->statusActionRepository->findAll();
        $actionStatusLabel = [];
        foreach ($actionStatus as $oneActionStatus) {
            $actionStatusLabel[$oneActionStatus->getLabel()] = $oneActionStatus->getId();
        }
        //configuration of choices Types
        $actionTypes = $this->typeActionRepository->findAll();
        $actionTypesLabel = [];
        foreach ($actionTypes as $actionType) {
            $actionTypesLabel[$actionType->getLabel()] = $actionType->getId();
        }
        //configuration of choices Périodes
        $dateFilter = [];
        $today = new DateTime();
        $dateFilter['aujourd\'hui'] = $today->setTime(0,0,0,0);
        //find the last day of the month to define how many days are necessary for interval "to do this month"
        $lastDayInMonth = new DateTime();
        $yearToAdd = date('Y');
        $monthToAdd = date('m');
        $dayToAdd = date('t');
        $lastDayInMonth = $lastDayInMonth->setDate($yearToAdd, $monthToAdd, $dayToAdd);
        $daysUntilEndOfMonth = $today->diff($lastDayInMonth)->d;

        $today = new DateTime();
        $toDoInTheNextSevenDays = $today->add(new DateInterval('P7D'));
        $dateFilter['sous 7 jours'] = $toDoInTheNextSevenDays->setTime(0,0,0,0);
        $today = new DateTime();
        $toDoInTheNextThirtyDays = $today->add(new DateInterval('P30D'));
        $dateFilter['sous 30 jours'] = $toDoInTheNextThirtyDays->setTime(0,0,0,0);
        $today = new DateTime();
        $toDoThisMonth = $today->add(new DateInterval('P'.$daysUntilEndOfMonth.'D'));
        $dateFilter['ce mois-ci'] = $toDoThisMonth->setTime(0,0,0,0);



        //creation of form to filter the actions
        $tasksFilterForm = $this->createFormBuilder()
            ->add('Plantes', ChoiceType::class, [
            'multiple' => true,
            'expanded' => true,
            'choices'  => $userPlantsName,
            'required' => false
            ])
            ->add('Statuts', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices'  => $actionStatusLabel,
                'required' => false
            ])
            ->add('Types', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices'  => $actionTypesLabel,
                'required' => false
            ])
            ->add('Periodes', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices'  => $dateFilter,
                'placeholder' => false,
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success']
            ])
            ->getForm();
        $tasksFilterForm->handleRequest($request);

        //if form is submitted and some choices are selected
        if ($tasksFilterForm->isSubmitted() && $tasksFilterForm->isValid()) {
            $data = $tasksFilterForm->getData();
            // verify if there are at least one filter
            if (!empty(array_filter($data, function ($a) {
                return $a !== null;
            }))) {
                $data = array_filter($data, function ($a) {
                    return $a !== null;
                });
                $data = $this->prepareActionsData($data, $userPlantsName, $actionStatusLabel, $actionTypesLabel, $dateFilter, $userId);
                $userActions = $this->actionRepository->searchByFilters($data);
            }
        }

        $popupForm = $this->createFormBuilder()
            ->add('plantsToValidate', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices'  => $newPlantsForm,
                'data' => $newPlantsForm])
            ->add('typePlantation', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices'  => ['semer' =>'sowing',
                    'planter' =>'plant',
                    'je ne sais pas encore' => 'other'],
                'data' => 'sowing'
            ])
            ->add('submit', SubmitType::class, ['label' => 'Valider'])
            ->getForm();
        $popupForm->handleRequest($request);

        if ($popupForm->isSubmitted() && $popupForm->isValid()) {
            $data = $popupForm->getData();
            $typePlantation = $data['typePlantation'];
            $plantsSelected = $data['plantsToValidate'];

            if($typePlantation === 'plant') {
                foreach($plantsSelected as $plantId) {
                    $this->addFirstActions($plantId, 'planter');
                    unset($newPlants[$plantId]);
                }
            } else {
                foreach($plantsSelected as $plantId) {
                    $this->addFirstActions($plantId, 'semer');
                    unset($newPlants[$plantId]);
                }
            }
        }

        return $this->render('userDashboard/tasks.html.twig', [
            'title' => 'Mes tâches',
            'userActions' => $userActions,
            'userGround' => $userGround,
            'tasksFilterForm' => $tasksFilterForm->createView(),
            'newPlants' => $newPlants,
            'tasksButtonDisplay' => $tasksButtonDisplay,
            'choicePlantationForm' => $popupForm->createView(),
            'imageNewPlants' => $imageNewPlants
        ]);
    }


    protected function prepareActionsData(array $data, $userPlantsName, $actionStatusLabel, $actionTypesLabel, $dateFilter, $userId)
    {
        $returnedData[]=[
            'idUser' => $userId
        ];

        if ($data['Plantes'] != null) {
            $returnedData[] = [
                'idPlant' => $data['Plantes']
            ];
        }else{
            foreach($userPlantsName as $userPlantId){
                $defaultPlant[] = $userPlantId;
            }
            $returnedData[] = [
                'idPlant' => $defaultPlant
            ];
        }

        if ($data['Statuts'] != null) {
            $returnedData[] = [
                'idStatusAction' => $data['Statuts']
            ];
        }else{
            foreach($actionStatusLabel as $actionStatusId){
                $defaultStatus[] = $actionStatusId;
            }
            $returnedData[] = [
                'idStatusAction' => $defaultStatus
            ];
        }

        if ($data['Types'] != null) {
            $returnedData[] = [
                'idTypeAction' => $data['Types']
            ];
        }else{
                foreach($actionTypesLabel as $actionTypeId){
                    $defaultType[] = $actionTypeId;
                }
                $returnedData[] = [
                    'idTypeAction' => $defaultType
                ];
        }

        if (isset($data['Periodes'])) {
            $dateRange = $this->getDateRange($dateFilter['aujourd\'hui'], $data['Periodes']);
            $returnedData[] = [
                'dateStart' => $dateRange
            ];
        }else{
            $queryMinDateStart = $this->actionRepository->findMinDateStart();
            $minDateStartString = explode("-", $queryMinDateStart[0][1]);
            $minDateStart = new DateTime();
            $minDateStart = $minDateStart->setDate($minDateStartString[0],$minDateStartString[1],$minDateStartString[2])->setTime(0,0,0,0);


            $queryMaxDateStart = $this->actionRepository->findMaxDateStart();
            $maxDateStartString = explode("-", $queryMaxDateStart[0][1]);
            $maxDateStart = new DateTime();
            $maxDateStart = $maxDateStart->setDate($maxDateStartString[0],$maxDateStartString[1],$maxDateStartString[2])->setTime(0,0,0,0);
            $dateRange = $this->getDateRange($minDateStart, $maxDateStart);
            $returnedData[] = [
                'dateStart' => $dateRange
            ];
        }

        return $returnedData;
    }

    protected function getDateRange($minDateStart, $maxDateStart): array
    {
        //calculate the interval between two dates
        $dayInterval = $minDateStart->diff($maxDateStart)->days;
        //create the range array and add the first day
        $range = [];
        $range[] = "'".$minDateStart->format('Y-m-d')."'";
        //stock the min date as string in a new variable
        $dateToConvert = $minDateStart->format('Y-m-d');
        //stock the year/month/day as single variables in an array
        $tempArray = explode("-", $dateToConvert);
        //for each day of the interval, we create a new date object and set the date with the variables in array. Then calculate interval + format and stock in range
        for($i = 1; $i <= $dayInterval; $i++){
            $date[$i] = new DateTime;
            $date[$i] = $date[$i]->setDate($tempArray[0], $tempArray[1], $tempArray[2]);
            $date[$i] = $date[$i]->add(new DateInterval('P'.$i.'D'))->format('Y-m-d');
            $range[] = "'".$date[$i]."'";
        }
        return $range;
    }


    public function addFirstActions($plantId, $typeActionLabel): Response
    {
        $action = $this->addAction($plantId, $typeActionLabel);

        /*--------------------------récupération-type-action--------------------------*/
        if ($typeActionLabel != 'planter') {
            $action = $this->addAction($plantId, 'planter');
        }

        $action = $this->addAction($plantId, 'récolter');
        $action = $this->addAction($plantId, 'arroser');

        return new Response(true);
    }

    public function dateManagement($plant, $monthStart, $monthEnd)
    {
        /*---------------------gestion-des-null-sur-la-plantation---------------------*/
        if ($plant->$monthStart() != null && $plant->$monthEnd() != null) {
            $monthStartRaw = $plant->$monthStart();
            $monthEndRaw = $plant->$monthEnd();
        } elseif ($plant->$monthStart() != null) {
            $monthStartRaw = $plant->$monthStart();
            $monthEndRaw = $monthStartRaw;
        } else {
            $currentMonth = idate('m');
            $monthStartRaw = $currentMonth;
            $monthEndRaw = $currentMonth;
        }
        /*----------------------------------------------------------------------------*/
        //Date conversion
        if ($monthEndRaw < 12) {
            $monthEndRaw += 1;
        } else {
            $monthEndRaw = 1;
        }
        /*------------------------Verification-année-plentation------------------------*/
        $currentMonth = idate('m');
        $currentYear = idate('Y');

        if ($monthStartRaw < $currentMonth) {
            $currentYear++;
        }

        if ($monthStartRaw === $currentMonth) {
            $dateStart = new DateTime();
        } else {
            $dateStart = new DateTime($currentYear . "-" . $monthStartRaw);
        }

        $currentYear = idate('Y');
        if ($monthEndRaw < $currentMonth) {
            $currentYear++;
        }

        //si le mois de départ est plus grand que le mois de fin, alors il faut augementer l'année de fin d'un an
        if ($monthStartRaw > $monthEndRaw) {
            $currentYear++;
        }

        $dateEnd = new DateTime($currentYear . "-" . $monthEndRaw);
        /*-----------------------------------------------------------------------------*/

        return [$dateStart, $dateEnd];
    }

    public function addAction($plantId, $typeActionLabel, $datesArray = null)
    {
        $newAddAction = new Action(); //? probablement changer le nom de variable 
        $user = $this->getUser();
        $typeAction = $this->typeActionRepository->findOneBy(['label' => $typeActionLabel]);
        $plant = $this->plantRepository->findOneBy(['id' => $plantId]);
        $statusActionId = 2;
        $statusAction = $this->statusActionRepository->findOneBy(['id' => $statusActionId]);

        /*----------------------récupération-du-label------------------------------*/
        $nameAction = "";

        switch ($typeActionLabel) {
            case 'planter':
                $nameAction = 'Transplanting';
                break;
            case 'semer':
                $nameAction = 'Sowing';
                break;
            case 'arroser':
                $nameAction = 'Water';
                $waterActionDate = $this->calculWater($plant);
                $dateStart = $waterActionDate;
                $dateEnd = $waterActionDate;
                break;
            case 'protéger':
                $nameAction = 'Protect';
                if($datesArray === null) {
                    throw new \Exception("Mistake with dateArray in the addAction parameters");
                } else {
                    $dateStart = $datesArray[0];
                    $dateEnd = $datesArray[1];
                }
                break;
            case 'récolter':
                $nameAction = 'Harvest';
                break;
        };
        /*--------------------------------------------------------------------------*/

        if (in_array($typeActionLabel, ['planter', 'semer', 'récolter'])) {
            $getMonthStart = 'get' . $nameAction . 'MonthStart';
            $getMonthEnd = 'get' . $nameAction . 'MonthEnd';

            $datesArray = $this->dateManagement($plant, $getMonthStart, $getMonthEnd);
            $dateStart = $datesArray[0];
            $dateEnd = $datesArray[1];
        }

        //Create new action
        $newAddAction->setIdUser($user);
        $newAddAction->setIdPlant($plant);
        $newAddAction->setDateStart($dateStart);
        $newAddAction->setDateEnd($dateEnd);
        $newAddAction->setIdTypeAction($typeAction);
        $newAddAction->setIdStatusAction($statusAction);
        $this->entityManager->persist($newAddAction);
        $this->entityManager->flush();
        return new Response(true);
    }

    public function calculWater($plant)
    {
        $waterNeeds = $plant->getWaterNeeds();

        $dateWaterAction = new DateTime();

        switch ($waterNeeds) {
            case 'faible':
                $dateWaterAction->add(new \DateInterval('P15D'));
                break;
            case 'moyen':
                $dateWaterAction->add(new \DateInterval('P7D'));
                break;
            case 'élevé':
                $dateWaterAction->add(new \DateInterval('P3D'));
                break;
        }

        return $dateWaterAction;
    }

    public function verifcationEvent($ground, $postalCode)
    {
        foreach($ground->getPlant() as $plant) {
            $this->addAction($plant->getId(),'protéger', [new DateTime(), new DateTime()]);
        }

        //get latitude and longitude of the potager of the user
        $responseLocation = $this->client->request(
            'GET',
            'https://api.openweathermap.org/data/2.5/weather?zip='. $postalCode .',FR&appid=8e602b9ea28ed4f9f8fc97a5f6d1105c'
        );

        if ($responseLocation->getStatusCode() != 200) {
            throw new \Exception("problème avec la requète");
        }
        $contentLocation = $responseLocation->toArray();
        $lon = $contentLocation["coord"]["lon"];
        $lat = $contentLocation["coord"]["lat"];

        //get the event
        $response = $this->client->request(
            'GET',
               'https://api.openweathermap.org/data/2.5/onecall?lat='. $lat .'&lon=' . $lon . '&exclude=hourly,daily,current,minutely&appid=e87eecb19630c52f9fe4729b6a88612a'
        );

        $content = $response->toArray();

        if(isset($content["alerts"])) {

            // If it's freezing
            if (str_contains($content["alerts"][0]["event"], "freezing rain")) {

                $eventDateStart = (new DateTime())->setTimestamp($content["alerts"][0]["start"])->settime(0,0);
                $eventDateEnd = (new DateTime())->setTimestamp($content["alerts"][0]["end"])->settime(0,0);

                $eventDates = [$eventDateStart, $eventDateEnd];

                // Est-ce qu'on creer une action pour toutes les plantes ou juste une générale ?
                foreach($ground->getPlant() as $plant) {
                    $this->addAction($plant->getId(),'protéger', $eventDates);
                }
            }
        }
    }
}

