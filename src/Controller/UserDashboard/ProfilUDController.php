<?php

namespace App\Controller\UserDashboard;

use App\Entity\City;
use App\Repository\CityRepository;
use App\Repository\GroundRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ProfilUDController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/dashboard/profil', name: 'profil')]
    public function editProfile(Request $request, UserPasswordHasherInterface $passwordEncoder, CityRepository $cityRepository, SluggerInterface $slugger, GroundRepository $groundRepository): Response
    {
        //Activation de l'onglet Tâches
        $user = $this->getUser();
        $ground = $groundRepository->findOneBy(['user' => $user]);
        if($ground != null){
            $tasksButtonDisplay = true;
        }else{
            $tasksButtonDisplay = false;
        }

        if ($request->isMethod('POST')) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            //Verify if avatar has been changed
            if ($request->files->get('newAvatar') != null) {
                $avatar = $request->files->get('newAvatar');
                $safeFilename = $slugger->slug($avatar);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $avatar->guessExtension();
                // Move the file to the directory where brochures are stored
                try {
                    $avatar->move($this->getParameter('avatars_directory'), $newFilename);
                } catch (FileException $e) {
                    $this->addFlash('error', 'Impossible de mettre votre avatar à jour. Merci de réessayer.');
                }
                //Delete old avatar file if available
                if ($user->getAvatar() != null) {
                    $oldAvatar = $user->getAvatar();
                    unlink($_SERVER["DOCUMENT_ROOT"] . 'uploads/avatars/' . $oldAvatar);
                }
                $user->setAvatar($newFilename);
                $this->addFlash('message', 'Avatar mis à jour');
            }


            //Verify if pseudo has been changed
            if ($request->request->get('newPseudo') != null) {
                //clean the pseudo entry
                $pseudo = ucfirst(strip_tags($request->request->get('newPseudo')));
                $user->setPseudo($pseudo);
                $this->addFlash('message', 'Pseudo mis à jour');
            }

            //Verify if password has been changed
            if ($request->request->get('newPassword') != null && $request->request->get('oldPassword') != null) {
                //Verify if old password (database) and old password (input) are identical
                if ($passwordEncoder->isPasswordValid($user, $request->request->get('oldPassword'))) {
                    $newPassword = $request->request->get('newPassword');
                    //Verify if the two passwords are identical
                    if ($newPassword == $request->request->get('newPassword2')) {
                        $user->setPassword($passwordEncoder->hashPassword($user, $newPassword));
                        $this->addFlash('message', 'Mot de passe mis à jour');
                    } else {
                        $this->addFlash('error', 'Les deux mots de passe ne sont pas identiques.');
                    }
                } else {
                    $this->addFlash('error', 'Votre actuel mot de passe ne correspond pas au mot de passe renseigné.');
                }
            }

            //Verify if city name and postal code have been changed
            if ($request->request->get('newCityName') != null) {
                if ($request->request->get('newCityPostalCode') != null) {
                    //Get the city of the logged in user
                    $city = $user->getCity();
                    //Clean the city inputs
                    $newCityName = ucfirst(strip_tags($request->request->get('newCityName')));
                    $newCityPostalCode = ucfirst(strip_tags($request->request->get('newCityPostalCode')));
                    //Look if the new city name already is in database
                    $foundedCity = $cityRepository->findOneBy(['name' => $newCityName]);
                    if ($foundedCity !== null) {
                        //If in database, use the city in database
                        $city = $foundedCity;
                    } else {
                        //If not in database, create a new city
                        $city = new City();
                        $city->setName($newCityName);
                        $city->setPostalCode($newCityPostalCode);
                    }
                    //Modify the city for the logged in user
                    $user->setCity($city);

                    $this->addFlash('message', 'Ville mise à jour');
                } else {
                    $this->addFlash('error', 'Veuillez sélectionner une ville parmis celles proposées.');
                }
            }
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('profil');
        }
        return $this->render('userDashboard/profil.html.twig', [
            'title' => 'Mon profil',
            'tasksButtonDisplay' => $tasksButtonDisplay,
        ]);
    }
}
