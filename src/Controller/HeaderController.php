<?php
namespace App\Controller;

use App\Repository\PlantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HeaderController extends AbstractController
{
    #[Route('/navbar', name: 'navbar')]
    public function index(PlantRepository $plantRepository, Request $request): Response
    {
        //generate the form for the serachbar in the header
        $plants = [];

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('resultsSearch'))
            ->add('searchTerm', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'attr' => ['placeholder' => 'Cherchez un legume ou un fruit'],
                'label' => false
            ])
            ->add('submit', SubmitType::class, ['label' => false])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            return $form->getData();
        }

        $autocomplete = [];

        foreach ($plantRepository->findAll() as $plant) {
            $autocomplete[] = $plant->getName();
        }

        return $this->render('theme/header.html.twig', [
            'plantsHeader' => $plants,
            'autocompleteHeader' => $autocomplete,
            'formHeader' => $form->createView()
        ]);
    }
}
