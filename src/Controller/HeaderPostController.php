<?php

namespace App\Controller;


use App\Repository\ArticleRepository;
use App\Repository\PlantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HeaderPostController extends AbstractController
{
    #[Route('/resultsSearch', name: 'resultsSearch')]
    public function index(Request $request, PlantRepository $plantRepository, ArticleRepository $articleRepository): Response
    {
        $formData = $request->get('form');
        $plantName = $formData['searchTerm'];

        $plants = $plantRepository->searchByName($plantName);
        $articles = $articleRepository->findByPlant($plantName);

        return $this->render('results_search/index.html.twig', [
            'controller_name' => 'ResultsSearchController',
            'plants' => $plants,
            'articles' => $articles
        ]);
    }

}