<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TutoController extends AbstractController
{
    #[Route('/tuto', name: 'tuto')]
    public function index(ArticleRepository $repository): Response
    {
        return $this->render('tuto/index.html.twig', [
            'controller_name' => 'TutoController',
            'articles' => $repository->findAll()
        ]);
    }
}
