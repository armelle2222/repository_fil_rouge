<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use App\Repository\CityRepository;
use App\Security\EmailVerifier;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegisterController extends AbstractController
{
    private $emailVerifier;
    private $entityManager;

    public function __construct(EmailVerifier $emailVerifier, EntityManagerInterface $entityManager)
    {
        $this->emailVerifier = $emailVerifier;
        $this->entityManager = $entityManager;
    }

    #[Route('/register', name: 'register')]
    public function register(Request $request, UserPasswordHasherInterface $passwordEncoder, CityRepository $cityRepository): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $city = $user->getCity();

            if(empty($city->getPostalCode())) {
                throw new \Exception("Veuillez séléctionner une ville parmis celles proposées");
            }

            $cityName = $user->getCity()->getName();
            $city->setName(ucfirst(strip_tags($cityName)));

            // clean the city entry and verify if it already exists
            $foundedCity = $cityRepository->findOneBy(['name' => $cityName]);

            if ($foundedCity !== null) {
                $city = $foundedCity;
            }

            $user->setCity($city);

            if ($form->isValid()) {
                $role = "ROLE_USER";
                $user->setRole($role);

//                 clean the pseudo entry
                $pseudo = ucfirst(strip_tags($form->get('pseudo')->getData()));
                $user->setPseudo($pseudo);

                // encode the plain password
                $user->setPassword(
                    $passwordEncoder->hashPassword(
                        $user,
                        $form->get('password')->getData()
                    )
                );

                $this->entityManager->persist($user);
                $this->entityManager->flush();

                // generate a signed url and email it to the user
                $this->emailVerifier->sendEmailConfirmation(
                    'app_verify_email',
                    $user,
                    (new TemplatedEmail())
                        ->from(new Address('poussvert2021@gmail.com', 'Pouss\'Vert'))
                        ->to($user->getEmail())
                        ->subject('Confirmation de votre e-mail')
                        ->htmlTemplate('register/confirmation_email.html.twig')
                );

                return $this->redirectToRoute('dashboard');
            }
        }
        return $this->render('register/index.html.twig', [
            'title' => 'Inscription',
            'form' => $form->createView()
        ]);
    }


    #[Route('/verify/email', name: 'app_verify_email')]
    public function verifyUserEmail(Request $request, UserRepository $userRepository): Response
    {
        $id = $request->get('id');

        if ($id === null) {
            return $this->redirectToRoute('register');
        }

        $user = $userRepository->find($id);

        if ($user === null) {
            return $this->redirectToRoute('register');
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Votre e-mail a bien été vérifié.');

        return $this->redirectToRoute('home');
    }
}
