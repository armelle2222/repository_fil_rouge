<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @ORM\Table(name="action", indexes={@ORM\Index(name="action_plant0_FK", columns={"id_plant"}), @ORM\Index(name="action_status_action2_FK", columns={"id_status_action"}), @ORM\Index(name="action_user_FK", columns={"id_user"}), @ORM\Index(name="action_type_action1_FK", columns={"id_type_action"})})
 * @ORM\Entity(repositoryClass="App\Repository\ActionRepository")
 */
class Action
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ?DateTime
     *
     * @ORM\Column(name="date_start", type="date", nullable=false)
     */
    private $dateStart;

    /**
     * @var ?DateTime
     *
     * @ORM\Column(name="date_end", type="date", nullable=false)
     */
    private $dateEnd;

    /**
     * @var ?Plant
     *
     * @ORM\ManyToOne(targetEntity="Plant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plant", referencedColumnName="id")
     * })
     */
    private $idPlant;

    /**
     * @var ?StatusAction
     *
     * @ORM\ManyToOne(targetEntity="StatusAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_status_action", referencedColumnName="id")
     * })
     */
    private $idStatusAction;

    /**
     * @var ?TypeAction
     *
     * @ORM\ManyToOne(targetEntity="TypeAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_action", referencedColumnName="id")
     * })
     */
    private $idTypeAction;

    /**
     * @var ?User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return ?DateTime
     */
    public function getDateStart(): ?DateTime
    {
        return $this->dateStart;
    }

    /**
     * @param ?DateTime $dateStart
     */
    public function setDateStart(?DateTime $dateStart): void
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return ?DateTime
     */
    public function getDateEnd(): ?DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @param ?DateTime $dateEnd
     */
    public function setDateEnd(?DateTime $dateEnd): void
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return ?Plant
     */
    public function getIdPlant()
    {
        return $this->idPlant;
    }

    /**
     * @param ?Plant $idPlant
     */
    public function setIdPlant(?Plant $idPlant): void
    {
        $this->idPlant = $idPlant;
    }

    /**
     * @return ?StatusAction
     */
    public function getIdStatusAction(): ?StatusAction
    {
        return $this->idStatusAction;
    }

    /**
     * @param ?StatusAction $idStatusAction
     */
    public function setIdStatusAction(?StatusAction $idStatusAction): void
    {
        $this->idStatusAction = $idStatusAction;
    }

    /**
     * @return ?TypeAction
     */
    public function getIdTypeAction(): ?TypeAction
    {
        return $this->idTypeAction;
    }

    /**
     * @param ?TypeAction $idTypeAction
     */
    public function setIdTypeAction(?TypeAction $idTypeAction): void
    {
        $this->idTypeAction = $idTypeAction;
    }

    /**
     * @return ?User
     */
    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    /**
     * @param ?User $idUser
     */
    public function setIdUser(?User $idUser): void
    {
        $this->idUser = $idUser;
    }
}
