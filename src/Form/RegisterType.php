<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pseudo', TextType::class, ['attr' => ['placeholder' => 'Entrez votre pseudonyme'], 'label' => false])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de passe doivent être identiques',
                'required' => true,
                'options' => [
                    'constraints' =>
                    new Assert\Regex([
                        'pattern' => '#^((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$#',
                        'message' => 'Votre mot de passe doit être composé de 8 caractères minimum, dont une lettre majuscule, une lettre   minuscule, un chiffre et un caractère spécial (!@#$&*).'
                    ]),
                ],
                'constraints' => new Length(null, 8, 30),
                'first_options'  => ['attr' => ['placeholder' => 'Entrez votre mot de passe', 'class' => 'exclude'], 'label' => false],
                'second_options' => ['attr' => ['placeholder' => 'Confirmez votre mot de passe', 'class' => 'exclude'], 'label' => false]
            ])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Entrez votre e-mail'], 'label' => false])
            ->add('city', CityType::class)
            ->add('submit', SubmitType::class, ['label' => 'S\'inscrire']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
