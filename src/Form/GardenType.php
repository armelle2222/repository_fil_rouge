<?php

namespace App\Form;

use App\Entity\Plant;
use App\Entity\Ground;
use App\Entity\GroundType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class GardenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('width', IntegerType::class, ['attr' => ['placeholder' => 'Largeur de votre potager'], 'label' => false])
            ->add('height', IntegerType::class, ['attr' => ['placeholder' => 'Longueur de votre potager'], 'label' => false])
            ->add('user', HiddenType::class)
            ->add('groundType', EntityType::class, [
                'class' => GroundType::class,
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
                'label' => false,
                'by_reference'  => false,
            ])
            ->add('plant', EntityType::class, [
                'class' => Plant::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
                'label' => false,
                'by_reference'  => false,
            ])
            ->add('submit', SubmitType::class, ['label' => 'Confirmer']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ground::class,
        ]);
    }
}
