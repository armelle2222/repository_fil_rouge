const key = "50f0935292e1b1d40f75d87f696b379a&lang=fr&units=metric";

const weatherIcons = {//todo     ce sont les choses les plu souvent renvoyer
  Rain: "wi wi-day-rain",
  Clouds: "wi wi-day-cloudy",
  Clear: "wi wi-day-sunny",
  Snow: "wi wi-day-snow",
  mist: "wi wi-day-fog",
  Drizzle: "wi wi-day-sleet",
};

function capitalize(str) {
  return str[0].toUpperCase() + str.slice(1);
}

async function main() {
  const ip = await fetch("https://api.ipify.org?format=json")
    .then((resultat) => resultat.json())
    .then((json) => json.ip);

  const country_code = await fetch("https://freegeoip.app/json/" + ip)
    .then((resultat) => resultat.json())
    .then((json) => json.country_code);

  const zip_code = await fetch("https://freegeoip.app/json/" + ip)
    .then((resultat) => resultat.json())
    .then((json) => json.zip_code);

    const meteo = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?zip=${zip_code},${country_code}&appid=${key}`
        )
        .then((resultat) => resultat.json())
    .then((json) => json);

    weatherInfos(meteo);
}

function weatherInfos(data) {
    const name = data.name;
    const temperature = data.main.temp;
    const felling = data.main.feels_like;
    const pressure = data.main.pressure;
    const humidity = data.main.humidity;
    const conditions = data.weather[0].main;
    const description = data.weather[0].description;
    const speed = data.wind.speed;
    const gust = data.wind.gust;
    const deg = data.wind.deg;
  
  function sensDuVent(deg) {
    if (deg > 337.5) return 'Nord';
    if (deg > 292.5) return 'Nord-Ouest';
    if (deg > 247.5) return 'Ouest';
    if (deg > 202.5) return 'Sud-Ouest';
    if (deg > 157.5) return 'Sud';
    if (deg > 122.5) return 'Sud-Est';
    if (deg > 67.5) return 'Est';
    if (deg > 22.5) return 'Nord-Est';
}

    document.querySelector("#villeWidget").textContent = name;
    document.querySelector("#temperatureWidget").textContent = Math.round(temperature) + " °C";
    document.querySelector("i.wi").className = weatherIcons[conditions];

    document.querySelector("#temperature").textContent = Math.round(temperature) + " °C";
    document.querySelector("#ville").textContent = name;
    document.querySelector("#felling").textContent = Math.round(felling) + " °C";
    document.querySelector("#pressure").textContent = Math.round(pressure) + " hPa";
    document.querySelector("#humidity").textContent = Math.round(humidity) + " %";
    document.querySelector("#description").textContent = capitalize(description);
    document.querySelector("#windDirection").textContent = capitalize(sensDuVent(deg));
    document.querySelector("#windSpeed").textContent = Math.round(speed) + " km/h";
    document.querySelector("#windGuest").textContent = Math.round(gust) + " km/h";
}

main();


